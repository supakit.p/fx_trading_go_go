package routes

import (
	"net/http"
    "github.com/gin-gonic/gin"
)

func RoutePath(r *gin.Engine) {
    r.GET("/", Index)
}

func Index(context *gin.Context) {
    context.JSON(http.StatusOK, "Hello World")
}
