package routes

import (
    "github.com/gin-gonic/gin"
    "../controller"
)

// path /example
func RouteExampleGroup(r *gin.RouterGroup) {
        r.GET("/get_query_params", controller.ExGetParams) // or can puy Parameters in path /example/get_query_params/:name/*action
        r.POST("/post_url_encode", controller.ExPostURL)
        r.POST("/post_json", controller.ExPostJson)
}