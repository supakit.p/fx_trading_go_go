package controller

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"../internal/model"
)

func ExPostJson(context *gin.Context) {
    var request model.Name
    err := context.ShouldBindJSON(&request)
    if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
    }
    context.JSON(http.StatusOK, "Hello Kuy "+ request.FirstName + " " + request.LastName)
}

func ExGetParams(context *gin.Context) {
	username := context.Query("username") 
	password := context.Query("password") 
    if username == "" {
		context.JSON(http.StatusBadRequest, gin.H{"error":"username NotFound"})
		return
    }
    context.JSON(http.StatusOK, "Get "+ username + " password " + password)
}

func ExPostURL(context *gin.Context) {
	id := context.PostForm("id") 
	name := context.PostForm("name") 
    if id == "" {
		context.JSON(http.StatusBadRequest, gin.H{"error":"id is null"})
		return
    }
    context.JSON(http.StatusOK, "Hello id "+ id + " Name " + name)
}