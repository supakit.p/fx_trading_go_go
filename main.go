package main
import (
//    "fmt" // package set fomat input||output
//    "net/http"
   "log"
    "./app/routes"
   "github.com/gin-gonic/gin"
)

func main() {
    router := gin.Default()
    routes.RoutePath(router)
    routes.RouteExampleGroup(router.Group("/example"))
    log.Fatal(router.Run())
}